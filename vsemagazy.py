from urllib.request import urlopen # for Python 3
from lxml.etree import XMLSyntaxError
from lxml.html import fromstring
from pandas import DataFrame, ExcelWriter
import requests
URL = 'http://www.vsemagazy.ru/'
KATEGORY_PATH = '.menu li '
MAGAZ_PATH = '.tbl-shops tr'
ADRES_PATH = '.tbl-shops'
ADR_PATH = '.post p b'
SUBJ_PATH = '.post p'
def vsemagazy_parse():
    f = requests.get('http://www.vsemagazy.ru/')
    list_html = f.text
    list_doc = fromstring(list_html)
    df = DataFrame(columns=('магазин', 'область', 'субъект', 'график работы', 'телефон', 'адрес'))

    for kategory in list_doc.cssselect(KATEGORY_PATH):
        a = kategory.cssselect('a')[0]
        href = a.get('href')
        name = a.text
 
        details_html = requests.get(href).text
        try:
            details_doc = fromstring(details_html)
        except XMLSyntaxError:
             continue
        for magaz in details_doc.cssselect(MAGAZ_PATH):
            b = magaz.cssselect('a')[0]
            zref = b.get('href')
            namb = b.text
            
            drog_html = requests.get(zref).text
            try:
                dret_doc = fromstring(drog_html)
            except XMLSyntaxError:
                continue

            for adres in dret_doc.cssselect(ADRES_PATH):
                c = adres.cssselect('a')[0]
                nref = c.get('href')
                band = c.text

                drt_html = requests.get(nref).text
                try:
                    dry_doc = fromstring(drt_html)
                except XMLSyntaxError:
                    continue

                subjekt = dry_doc.cssselect(SUBJ_PATH)[0].text_content()
                grafik = dry_doc.cssselect(SUBJ_PATH)[1].text_content()
                telephone = dry_doc.cssselect(SUBJ_PATH)[2].text_content()
                adres = dry_doc.cssselect(SUBJ_PATH)[3].text_content()
                haret_elems_list = [('магазин', name), ('область', namb), ('субъект', subjekt), ('график работы',grafik), ('телефон', telephone), ('адрес', adres)]
                print(haret_elems_list)
                df = df.append(dict(haret_elems_list), ignore_index=True)
    writer = ExcelWriter('magaz.xlsx', engine='xlsxwriter')
    df.to_excel(writer, sheet_name='1', header=True, index=False)
    writer.save()           

def main():
    vsemagazy_parse()

if __name__ == '__main__':
    main()
