from lxml.html import fromstring
from urllib.parse import urljoin
from pandas import DataFrame, ExcelWriter
import requests

URL = 'http://luminarc.su/'
KATEGORY_PATH = '#MenuTop ul li ul li'
PRODUCT_PATH = '#ShopElements div a'
KAT_PATH = '.area a'
ZAG_PATH = '.area span'
IMG = '.big'
IMAGE_PATH = '.mini'
OPISANIEONE = '.right .description'
OPISANIETWO = '.manufacturer'
PRICESEGODNYA = '.buy span span'
PRICESTAR = '.buy p span'
ARTICRUL = '.big span b'
XARAKTIRISTIKI = '.options ul li'
ZEALOT = '#ShopElement h1'


def luminarc_parse():
    f = requests.get(URL).text
    g = fromstring(f)
    df = DataFrame(columns=('категория1', 'категория2', 'категория3', 'загаловок'))
    for katey in g.cssselect(KATEGORY_PATH):
        a = katey.cssselect('a')[0]
        href = a.get('href')
        nrav = urljoin(href, '?page=all')
        silkaone = requests.get(nrav).text
        silkaone_list = fromstring(silkaone)

        for tovari in silkaone_list.cssselect(PRODUCT_PATH):
            b = tovari.cssselect('a')[0]
            bhref = b.get('href')
            try:
                silvano = requests.get(bhref).text
                silkatwo_list = fromstring(silvano)
            except requests.exceptions.MissingSchema:
                silvano = ''

            katone = silkatwo_list.cssselect(KAT_PATH)[1].text_content()
            kattwo = silkatwo_list.cssselect(KAT_PATH)[2].text_content()

            try:
                katthree = silkatwo_list.cssselect(KAT_PATH)[3].text_content()
            except IndexError:
                katthree = ''

            zagalovok = silkatwo_list.cssselect(ZEALOT)[0].text_content()

            for image in silkatwo_list.cssselect(IMG):
                u = image.cssselect('img')[0]
                un = u.get('src')
                unn = urljoin(URL, un)
                t = requests.get(unn)
                k = open('image/%s' % un.split('/')[-1], 'wb')
                k.write(t.content)
                k.close()

            for imagedop in silkatwo_list.cssselect(IMAGE_PATH):
                i = imagedop.cssselect('a')[1]
                im = i.get('href')
                r = requests.get(im)
                l = open('image/%s' % im.split('/')[-1], 'wb')
                l.write(r.content)
                l.close()
            try:
                opisanione = silkatwo_list.cssselect(OPISANIEONE)[0].text_content()
            except IndexError:
                opisanione = ''
            try:
                opisanietwo = silkatwo_list.cssselect(OPISANIETWO)[0].text_content()
            except IndexError:
                opisanietwo = ''
            pricesegonya = silkatwo_list.cssselect(PRICESEGODNYA)[0].text_content()
            pricestar = silkatwo_list.cssselect(PRICESTAR)[0].text_content()
            articul = silkatwo_list.cssselect(ARTICRUL)[0].text_content()
            limuanars_elems = [('категория1', katone),
                               ('категория2', kattwo),
                               ('категория3', katthree),
                               ('загаловок', zagalovok),
                               ('картинка', k),
                               ('картинка дополнительная', l),
                               ('описание один', opisanione),
                               ('описаине два', opisanietwo),
                               ('цена сегодня', pricesegonya),
                               ('цена старая', pricestar),
                               ('артикул', articul)]
            for xaraktistiki in silkatwo_list.cssselect(XARAKTIRISTIKI):
                xarizag = xaraktistiki.cssselect('i')[0]
                xarizagg = xarizag.text
                xarinam = xaraktistiki.cssselect('b')[0]
                xarinamm = xarinam.text
                limuanars_elems.append((xarizagg, xarinamm))
            df = df.append(dict(limuanars_elems), ignore_index=True)
            print(df)

        writer = ExcelWriter('luminarc.xlsx', engine='xlsxwriter')
        df.to_excel(writer, sheet_name='1', header=True, index=False)
        writer.save


def main():
    luminarc_parse()


if __name__ == '__main__':
    main()
