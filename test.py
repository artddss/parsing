import requests
from lxml.html import fromstring

URL = 'http://luminarc.su/stolovye-servizy/?page=all'
TOVARI_PATH = '#ShopElements div a'


def parse():
    f = requests.get(URL).text
    r = fromstring(f)

    for tovari in r.cssselect(TOVARI_PATH):
        a = tovari.cssselect('a')[0]
        href = a.get('href')
        name = a.text
        print(href, name)


def main():
    parse()


if __name__ == '__main__':
    main()
