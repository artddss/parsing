from lxml.etree import XMLSyntaxError
from lxml.html import fromstring
from pandas import DataFrame, ExcelWriter
from urllib.parse import urljoin
import requests

URL = 'http://www.jessnail.ru/'
KATEY_PATH = ".inner-wrap .popup-block li"
KATA_PATH = '.category-item'
TOVARI_PATH = '.item-wrap .cat-main-item'
PAGINATION_PATH = '.pages'
ZAGALOVOK_PATH = '.page h1'


def jessa_parse():
    f = requests.get(URL).text
    list_doc = fromstring(f)

    for kategory in list_doc.cssselect(KATEY_PATH):
        a = kategory.cssselect('a')[0]
        href = a.get('href')
        urle = urljoin(URL, href)

        tovar = requests.get(urle).text
        tovarr = fromstring(tovar)

        for tova in tovarr.cssselect(TOVARI_PATH):
            b = tova.cssselect('a')[0]
            bref = b.get('href')
            nref = urljoin(URL, bref)

            atora = requests.get(nref).text
            sora = fromstring(atora)

            for paginations in sora.cssselect(PAGINATION_PATH):
                d = paginations.cssselect('a')[0]
                dref = d.get('href')
                print(dref)


def main():
    jessa_parse()


if __name__ == '__main__':
    main()
