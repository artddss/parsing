import requests
from urllib.request import urlopen
from lxml.etree import XMLSyntaxError
from lxml.html import fromstring
from pandas import DataFrame, ExcelWriter
from urllib.parse import urljoin
URL = 'http://valta.ru/catalog/'
TOVARI_PATH = '.name_wrapp'
ZAGALOVOK_PATH = '.prod-name b'
ART_PATH = '.prod-art'
OPISANIE_PATH = '.product-description div'
PRICE_PATH = '.total-price'
SOSTAV_PATH = '.product-extra-info .content'
IMAGE_PATH = '.gallery'
IMAGA_PATH = '.product-photo'
XARIKI_SILKA_PATH = '.main-detals div a'
XARIKI_BEZ_SILKA_PATH = '.main-detals div span'
KOLICHESTVO_PATH = '.product-availability span'
def nostrou_parse():
	f = urlopen(URL)
	list_html = f.read().decode('utf-8')
	list_doc = fromstring(list_html)

	df = DataFrame(columns=('загаловок', 'артикул'))

	for tovari in list_doc.cssselect(TOVARI_PATH):
		a = tovari.cssselect('a')[0]
		href = a.get('href')
		name = a.text
		silka = urljoin(URL, href)
		tovar = requests.get(silka).content.decode('utf-8')
		#tovar = urlopen(silka).read().decode('utf-8')
		tova = fromstring(tovar)

		zagalovok = tova.cssselect(ZAGALOVOK_PATH)[0].text_content()
		articul = tova.cssselect(ART_PATH)[0].text_content()
		opisanie = tova.cssselect(OPISANIE_PATH)[1].text_content()
		try:
			price = tova.cssselect(PRICE_PATH)[0].text_content()
		except IndexError:
			price = 'Нет в продаже'
		try:
			sostav = tova.cssselect(SOSTAV_PATH)[0].text_content()
		except IndexError:
			sostav = '-'
		try:
			indigrient = tova.cssselect(SOSTAV_PATH)[0].text_content()
		except IndexError:
			indigrient = '-'
		
		try:
			for image in tova.cssselect(IMAGE_PATH):
				b = image.cssselect('a')[0]
				im = b.get('href')
				ine = urljoin(URL, im)
				r = requests.get(ine)
				l = open('/home/artddss/image/%s' % im.split('/')[-1], 'wb')
				l.write(r.content)
				l.close
		except IndexError:
				im = ''

		nazvanie_brend = tova.cssselect(XARIKI_SILKA_PATH)[0].text
		proizvodtiel = tova.cssselect(XARIKI_BEZ_SILKA_PATH)[0].text_content()
		kategoria = tova.cssselect(XARIKI_BEZ_SILKA_PATH)[1].text_content()
		try:
			strana = tova.cssselect(XARIKI_BEZ_SILKA_PATH)[2].text_content()
		except IndexError:
			strana = ''
		try:
			fasovka = tova.cssselect(XARIKI_BEZ_SILKA_PATH)[3].text_content()
		except IndexError:
			fasovka = ''
		try:
			ves = tova.cssselect(XARIKI_BEZ_SILKA_PATH)[4].text_content()
		except IndexError:
			ves = ''
		try:
			gabariti = tova.cssselect(XARIKI_BEZ_SILKA_PATH)[5].text_content()
		except IndexError:
			gabariti = ''
		try:
			strix = tova.cssselect(XARIKI_BEZ_SILKA_PATH)[6].text_content()
		except IndexError:
			strix = ''
		try:
			kolistvo = tova.cssselect(KOLICHESTVO_PATH)[0].text_content()
		except IndexError:
			kolistvo = ''

		haret_elems_list = [('загаловок', zagalovok), ('артикул', articul), ('описани', opisanie), ('цена', price), ('состав', sostav), ('индигриент', indigrient), ('image', l), ('Название бренда', nazvanie_brend), ('Производитель', proizvodtiel), ('Категория', kategoria), ('Страна производства', strana), ('Фасовка', fasovka), ('Вес', ves), ('Габариты', gabariti), ('Штрих-код', strix), ('количество', kolistvo)]
		print(haret_elems_list)
	
		df = df.append(dict(haret_elems_list), ignore_index=True)
	writer = ExcelWriter('valta.xlsx', engine='xlsxwriter')
	df.to_excel(writer, sheet_name='1', header=True, index=False)
	writer.save()
	

def main():
    nostrou_parse()

if __name__ == '__main__':
    main()
